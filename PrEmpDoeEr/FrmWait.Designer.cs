﻿namespace PrEmpDoeEr
{
	partial class FrmWait
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
      System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmWait));
      this.lblStatus = new System.Windows.Forms.Label();
      this.prgWait = new System.Windows.Forms.ProgressBar();
      this.SuspendLayout();
      // 
      // lblStatus
      // 
      this.lblStatus.AutoSize = true;
      this.lblStatus.Location = new System.Drawing.Point(9, 38);
      this.lblStatus.Name = "lblStatus";
      this.lblStatus.Size = new System.Drawing.Size(56, 13);
      this.lblStatus.TabIndex = 5;
      this.lblStatus.Text = "Working...";
      // 
      // prgWait
      // 
      this.prgWait.Location = new System.Drawing.Point(12, 12);
      this.prgWait.Name = "prgWait";
      this.prgWait.Size = new System.Drawing.Size(441, 23);
      this.prgWait.Style = System.Windows.Forms.ProgressBarStyle.Marquee;
      this.prgWait.TabIndex = 4;
      // 
      // FrmWait
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(463, 56);
      this.Controls.Add(this.lblStatus);
      this.Controls.Add(this.prgWait);
      this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
      this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
      this.MaximizeBox = false;
      this.MinimizeBox = false;
      this.Name = "FrmWait";
      this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
      this.Text = "FrmWait";
      this.ResumeLayout(false);
      this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Label lblStatus;
		private System.Windows.Forms.ProgressBar prgWait;
	}
}