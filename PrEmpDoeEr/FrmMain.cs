﻿using System;
using System.Data;
using System.Text;
using System.Windows.Forms;
using System.ComponentModel;
using System.IO;

using SSInc.Utilities.Data;
using SSInc.Utilities.Reporting;
using prempeval;

namespace PrEmpDoeEr
{
  public partial class FrmMain : Form
  {
    private string DbName;
    private OdbcHandler DbConn;
    private string ReportHeader;
    private FrmWait _WaitForm = new FrmWait();
    private DataTable DtLocation;
    private DataTable DtConGroup;
    private DataTable DtPayGroup;
    private DataRow DrCorpParam;

    public FrmMain(string[] args)
    {
      this.InitializeComponent();

      this.DbName = args[0];

      txtSaveLoc.Text = Environment.ExpandEnvironmentVariables(@"%USERPROFILE%\Desktop\");
    }

    public FrmWait WaitForm
    {
      get
      {
        if (this._WaitForm == null || this._WaitForm.Disposing || this._WaitForm.IsDisposed)
          this._WaitForm = new FrmWait();

        return this._WaitForm;
      }
    }

    private void FrmMain_Load(object sender, EventArgs e)
    {
      this.Visible = true;
      Application.DoEvents();
      Application.DoEvents();

      var currYear = 0;

      var bwLoad = new BackgroundWorker();
      bwLoad.DoWork += (s, dwea) =>
      {
        if (!this.ConnectDb())
          Environment.Exit(1);

        this.DtLocation = this.DbConn.GetTable("SELECT \"loc-code\" AS loccode, \"loc-desc\" AS locdesc, eircode FROM pub.location");
        this.DtConGroup = this.DbConn.GetTable("SELECT * FROM pub.codes WHERE grpcode = 'PAY' AND fldgrp = 'CONGROUP'");
        this.DtPayGroup = this.DbConn.GetTable("SELECT * FROM pub.codes WHERE grpcode = 'PAY' AND fldgrp = 'PAYGROUP'");
        this.DrCorpParam = this.DbConn.FindFirst("SELECT * FROM pub.corpparam");

        currYear = int.Parse(this.DrCorpParam["pryear"].ToString());
      };
      bwLoad.RunWorkerCompleted += (s, rwcea) =>
      {
        try { this.WaitForm.Close(); }
        catch { }

        for (int i = 3; i > 0; i--)
        {
          cbYear.Items.Add(currYear - i);
        }
        cbYear.Items.Add(currYear);
        cbYear.Items.Add(currYear + 1);
        cbYear.SelectedItem = currYear;

        rbAll.Checked = true;
        dtpTermDate.Enabled = tbExcludeTerm.Checked;

        SSInc.Utilities.WinForms.ComboBoxes.PopFromTable(cbLoc, this.DtLocation, "loccode", "locdesc", 4);
        SSInc.Utilities.WinForms.ComboBoxes.AddItemAtTop(cbLoc, "-1", "* All Locations *");
        cbLoc.SelectedIndex = 0;

        SSInc.Utilities.WinForms.ComboBoxes.PopFromTable(cbEmpGrp, this.DtConGroup, "code", "codeDesc");
        SSInc.Utilities.WinForms.ComboBoxes.AddItemAtTop(cbEmpGrp, "-1", "* All Groups *");
        cbEmpGrp.SelectedIndex = 0;

        SSInc.Utilities.WinForms.ComboBoxes.PopFromTable(cbPayGrp, this.DtPayGroup, "code", "codeDesc");
        SSInc.Utilities.WinForms.ComboBoxes.AddItemAtTop(cbPayGrp, "-1", "* All Groups *");
        cbPayGrp.SelectedIndex = 0;
      };
      bwLoad.RunWorkerAsync();

      this.WaitForm.ShowDialog(this);
    }

    private void cbLoc_SelectedValueChanged(object sender, EventArgs e)
    {
      if (cbLoc.SelectedValue != null)
      {
        if (cbLoc.SelectedValue.ToString() != "-1")
        {
          tbLoc.Checked = false;
          tbLoc.Enabled = false;
        }
        else
          tbLoc.Enabled = true;
      }
    }

    private void tbExclude_CheckedChanged(object sender, EventArgs e)
    {
      dtpTermDate.Enabled = tbExcludeTerm.Checked;
    }

    private void btBrowse_Click(object sender, EventArgs e)
    {
      var fbdMain = new FolderBrowserDialog();
      if (fbdMain.ShowDialog(this) != DialogResult.OK)
        return;

      txtSaveLoc.Text = fbdMain.SelectedPath + (fbdMain.SelectedPath.EndsWith(@"\") ? "" : @"\");
    }

    private void btOk_Click(object sender, EventArgs e)
    {
      bwMain.RunWorkerAsync();

      this.WaitForm.SafeLabelText = "Loading data...";
      this.WaitForm.ShowDialog(this);
    }

    private void bwMain_DoWork(object sender, DoWorkEventArgs e)
    {
      var prYear = "";
      int empTot = 0;
      int empFile = 0;
      int empNon = 0;
      int currLoc = 0;
      var breakByLoc = false;
      var excludeByTermDate = false;
      var maxTermDate = "";
      var printMode = "ALL";
      var saveDir = "";

      var sbCsv = new StringBuilder();
      var sbPrint = new StringBuilder();

      var empQuery = "SELECT empno, location, contractgroup, firstname, lastname, spn, '' AS name FROM pub.empmstr WHERE paytype = '000' and YEAR(hiredate) < ";  //<=

      this.Invoke((MethodInvoker)delegate
      {
        prYear = cbYear.SelectedItem.ToString();
        empQuery += prYear;

        breakByLoc = tbLoc.Checked;
        excludeByTermDate = tbExcludeTerm.Checked;
        maxTermDate = (excludeByTermDate ? dtpTermDate.Value.ToString("MM/dd/yyyy") : "");

        printMode = (
          rbAll.Checked ? "ALL" :
          rbEvalYes.Checked ? "WITH" : "WITHOUT");

        if (cbLoc.SelectedValue.ToString() != "-1")
          empQuery += " AND Location = " + cbLoc.SelectedValue;

        if (cbEmpGrp.SelectedValue.ToString() != "-1")
          empQuery += " AND ContractGroup = '" + cbEmpGrp.SelectedValue + "'";

        if (cbPayGrp.SelectedValue.ToString() != "-1")
          empQuery += " AND PayGroup = '" + cbPayGrp.SelectedValue + "'";

        saveDir = txtSaveLoc.Text;
      });

      var dtEmps = this.DbConn.GetTable(empQuery);
      if (dtEmps.Rows.Count == 0)
      {
        MessageBox.Show(this, "No employees found with the specified criteria.");
        this.WaitForm.Close();
        this.Close();
      }
      else
      {
        this.WaitForm.SafeProgressBarMaximum = dtEmps.Rows.Count;
        this.WaitForm.SafeProgressBarStyle = ProgressBarStyle.Blocks;
        this.WaitForm.SafeLabelText = "Generating DOE-ER Reports...";
      }

      var dtEmpTerm = this.DbConn.GetTable("SELECT empno, termdate, rehiredate FROM pub.empterm ORDER BY empno, termdate DESC", true, false);
      var dtEmpEval = this.DbConn.GetTable("SELECT * FROM pub.empeval WHERE pryear = " + prYear);
      var dtEmpLeave = this.DbConn.GetTable("SELECT * FROM pub.EmpLeave WHERE ReturnDate IS NULL");
      var dtErTermCodes = this.DbConn.GetTable("SELECT * FROM pub.ertermcodes");

      this.ReportHeader =
        DateTime.Now.ToString("MM/dd/yyyy") + string.Empty.PadLeft(25) + "DOE-ER" + string.Empty.PadLeft(20) + Environment.NewLine +
        DateTime.Now.ToString("hh:mm tt").PadLeft(10) + string.Empty.PadLeft(25) + Environment.NewLine +
        "Name                                 SPN            Eval Code      Reason Code" + Environment.NewLine +
        "------------------------------------------------------------------------------" + Environment.NewLine;

      var evalLoc = this.GetValueFromCustParam("PAY-EvalModel").ToUpper(); // figuring out which location this is, each customer will require different formats

      foreach (var drPrint in dtEmps.Select("", (breakByLoc ? "Location, " : "") + "lastname, firstname"))
      {
        this.WaitForm.SafeProgressBarValue++;

        drPrint["name"] = drPrint["lastname"].ToString().Trim() + ", " + drPrint["firstname"].ToString().Trim() + " ";
        if (drPrint["name"].ToString().Length > 36)
          drPrint["name"] = drPrint["name"].ToString().Substring(0, 36);
        else
          drPrint["name"] = drPrint["name"].ToString().PadRight(36);

        var draEval = dtEmpEval.Select("empno = " + drPrint["empno"]);
        if (printMode == "WITH" && draEval.Length == 0)
          continue;
        if (printMode == "WITHOUT" && draEval.Length > 0)
          continue;

        if (evalLoc == "DUNELAND" && drPrint["spn"].ToString() == "0")
          continue;
        
        var draEmpTerm = dtEmpTerm.Select("empno = " + drPrint["empno"], "termdate DESC");
        var empMaxTerm = dtEmpTerm.Compute("MAX(termdate)", "empno = " + drPrint["empno"]);
        var empMaxHire = dtEmpTerm.Compute("MAX(rehiredate)", "empno = " + drPrint["empno"]);        

        bool isTermd = empMaxTerm != DBNull.Value && empMaxHire == DBNull.Value;
        if (empMaxTerm != DBNull.Value && empMaxHire != DBNull.Value && (DateTime)empMaxTerm > (DateTime)empMaxHire)
          isTermd = true;

        var termDate = (isTermd && draEmpTerm.Length > 0 ? ((DateTime)draEmpTerm[0]["termdate"]).ToString("MM/dd/yyyy") : "");

        if (excludeByTermDate && !string.IsNullOrEmpty(termDate) && DateTime.Parse(termDate) < DateTime.Parse(maxTermDate))
          continue;

        empTot++;

        if (breakByLoc && currLoc != (int)drPrint["Location"])
        {
          currLoc = (int)drPrint["Location"];
          sbPrint.Append(Environment.NewLine);

          var draLoc = this.DtLocation.Select("LocCode = " + drPrint["Location"]);;

          sbPrint.AppendLine((draLoc.Length > 0 ? draLoc[0]["locdesc"].ToString().Trim() : "Unknown Location"));
        }

        var drEval = (draEval.Length > 0 ? draEval[0] : null);

        sbPrint.Append(drPrint["Name"] + " ");
        sbPrint.Append(drPrint["SPN"].ToString().PadRight(15));

        if (drEval != null)
        {
          sbCsv.Append(this.DrCorpParam["DoeNum"] + ",");
          sbCsv.Append(drPrint["SPN"] + ",");
        }

        if (evalLoc == "WAYNE" && drEval != null)
        {
          decimal result = 0;
          #region * WAYNE *
          var wayneObj = new cWayne(drEval);
          empFile++;

          sbCsv.Append(wayneObj._Columns["EvaluationRating"]);
          sbPrint.Append(wayneObj._Columns["EvaluationRating"].PadLeft(20));
          
          if (wayneObj._Columns["EvaluationRating"] == "0")
          {
            decimal.TryParse(wayneObj._Columns["1stObsReason"].ToString(), out result);

            if (result != 0)
            {
              sbPrint.Append(result.ToString().PadLeft(17));
              sbCsv.Append("," + result);
            }
            else
            {
              sbPrint.Append("5".PadLeft(17));
              sbCsv.Append(",5");
            }


            //var draLeave = dtEmpLeave.Select("empno = " + drPrint["empno"]);

            //if (isTermd && draEmpTerm.Length > 0)
            //{
            //  var draErCross = dtErTermCodes.Select("FmsCode = '" + draEmpTerm[0]["TermCode"] + "'");
            //  if (draErCross.Length > 0)
            //  {
            //    sbPrint.Append(draErCross[0]["DoeCode"].ToString().PadLeft(17));
            //    sbCsv.Append("," + draErCross[0]["DoeCode"]);
            //  }
            //  else
            //  {
            //    sbPrint.Append("5".PadLeft(17));
            //    sbCsv.Append(",5");
            //  }
            //}
            //else if (draLeave.Length > 0)
            //{
            //  var draErCross = dtErTermCodes.Select("FmsCode = '" + draLeave[0]["Code"] + "' AND CodeType = 'LEAVE'");
            //  if (draErCross.Length > 0)
            //  {
            //    sbPrint.Append(draErCross[0]["DoeCode"].ToString().PadLeft(17));
            //    sbCsv.Append("," + draErCross[0]["DoeCode"]);
            //  }
            //  else
            //  {
            //    sbPrint.Append("5".PadLeft(17));
            //    sbCsv.Append(",5");
            //  }
            //}
            //else
            //{
            //  sbPrint.Append("5".PadLeft(17));
            //  sbCsv.Append(",5");
            //}
          }

          sbCsv.Append(Environment.NewLine);
          #endregion
        }
        else if (evalLoc == "SUNMAN" && drEval != null)
        {
          #region * SUNMAN *
          var sunmanObj = new cSunman(drEval);
          empFile++;

          sbCsv.Append(sunmanObj._Columns["EvaluationRating"]);
          sbPrint.Append(sunmanObj._Columns["EvaluationRating"].PadLeft(20));

          if (sunmanObj._Columns["EvaluationRating"] == "0")
          {
            if (isTermd && draEmpTerm.Length > 0)
            {
              var draErCross = dtErTermCodes.Select("FmsCode = '" + draEmpTerm[0]["TermCode"] + "'");
              if (draErCross.Length > 0)
              {
                sbPrint.Append(draErCross[0]["DoeCode"].ToString().PadLeft(17));
                sbCsv.Append("," + draErCross[0]["DoeCode"]);
              }
              else
              {
                sbPrint.Append("5".PadLeft(17));
                sbCsv.Append(",5");
              }
            }
            else
            {
              sbPrint.Append("5".PadLeft(17));
              sbCsv.Append(",5");
            }
          }

          sbCsv.Append(Environment.NewLine);
          #endregion
        }
        else if (evalLoc == "DUNELAND")
        {
          empFile++;

          sbPrint.Append("0".PadRight(15) + "5");

          sbCsv.AppendLine(this.DrCorpParam["doenum"] + "," + drPrint["spn"] + ",0,5");
        }
        else if (evalLoc != "SUNMAN" && evalLoc != "WAYNE" && drEval != null)
        {
          #region * EVERYONE ELSE *
          empFile++;
          sbPrint.Append(drEval["ColumnValues"]);
          sbCsv.Append(drEval["ColumnValues"]);

          if (drEval["ColumnValues"].ToString().Substring(0, drEval["ColumnValues"].ToString().Length - 1) == "0")
          {
            var draLeave = dtEmpLeave.Select("empno = " + drPrint["empno"]);

            if (isTermd && draEmpTerm.Length > 0)
            {
              var draErCross = dtErTermCodes.Select("FmsCode = '" + draEmpTerm[0]["TermCode"] + "' AND CodeType = 'TERM'");
              if (draErCross.Length > 0)
              {
                sbPrint.Append(draErCross[0]["DoeCode"].ToString().PadLeft(17));
                sbCsv.Append("," + draErCross[0]["DoeCode"]);
              }
              else
              {
                sbPrint.Append("5".PadLeft(17));
                sbCsv.Append(",5");
              }
            }
            else if (draLeave.Length > 0)
            {
              var draErCross = dtErTermCodes.Select("FmsCode = '" + draLeave[0]["Code"] + "' AND CodeType = 'LEAVE'");
              if (draErCross.Length > 0)
              {
                sbPrint.Append(draErCross[0]["DoeCode"].ToString().PadLeft(17));
                sbCsv.Append("," + draErCross[0]["DoeCode"]);
              }
              else
              {
                sbPrint.Append("5".PadLeft(17));
                sbCsv.Append(",5");
              }
            }
            else
            {
              sbPrint.Append("5".PadLeft(17));
              sbCsv.Append(",5");
            }
          }

          sbCsv.Append(Environment.NewLine);
          #endregion
        }
        else
        {
          empNon++;
          sbPrint.Append("No Eval Record".PadLeft(20));
        }

        sbPrint.Append(Environment.NewLine);
      }

      if (empTot > 1)
      {
        sbPrint.Append(Environment.NewLine);
        sbPrint.Append(empTot + " employees listed." + Environment.NewLine);
        sbPrint.Append(empFile + " employees included in file" + Environment.NewLine);
        sbPrint.Append(empNon + " employees without evaluations and not included in file");

        PdfDocument.BuildDocumentFromHeaderAndContent(this.ReportHeader, sbPrint.ToString(), "Courier New", 8, true);

        if (sbCsv.ToString() != string.Empty)
          File.WriteAllText(saveDir + "DOE_ER.csv", sbCsv.ToString());
      }
      else
        MessageBox.Show("No results found.");
    }

    private void bwMain_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
    {
      if (e.Error != null)
      {
        MessageBox.Show(
          this,
          "An error occurred while processing the DOE-ER report. Please contact your system's administrator." + 
          Environment.NewLine + Environment.NewLine + 
          e.Error.Message + 
          Environment.NewLine + Environment.NewLine + 
          e.Error.StackTrace);
      }

      try { this.WaitForm.Close(); }
      catch { }
    }

    private void btCancel_Click(object sender, EventArgs e)
    {
      this.Close();
    }

    public bool ConnectDb()
    {
      if (!Custom.Progress.TryGetHandlerFromController(this.DbName, out DbConn))
      {
        MessageBox.Show(
          "Missing Database connection information; cannot continue: " + Custom.Progress.LastError,
          this.Text, MessageBoxButtons.OK, MessageBoxIcon.Error);

        return false;
      }
      
      return true;
    }

    private string GetValueFromCustParam(string key)
    {
      var drTemp = this.DbConn.FindFirst("SELECT * FROM pub.\"cust-param\" WHERE \"reg-key\" = '" + key + "'");

      if (drTemp == null)
        return "";

      return drTemp["reg-value"].ToString().Trim();
    }    
  }
}
