﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace PrEmpDoeEr
{
	public partial class FrmWait : Form
	{
		public FrmWait()
		{
			InitializeComponent();
		}

		public string SafeLabelText
		{
			get
			{
				string cVal = "";

				if (this.InvokeRequired)
					this.Invoke((MethodInvoker)delegate() { cVal = lblStatus.Text; });
				else
					cVal = lblStatus.Text;

				return cVal;
			}
			set
			{
				if (this.InvokeRequired)
					this.Invoke((MethodInvoker)delegate() { this.SafeLabelText = value; });
				else
				{
					lblStatus.Text = value;
					Application.DoEvents();
				}
			}
		}

		public ProgressBarStyle SafeProgressBarStyle
		{
			get
			{
				ProgressBarStyle cVal = ProgressBarStyle.Blocks;

				if (this.InvokeRequired)
					this.Invoke((MethodInvoker)delegate() { cVal = prgWait.Style; });
				else
					cVal = prgWait.Style;

				return cVal;
			}
			set
			{
				if (this.InvokeRequired)
				{
					this.Invoke((MethodInvoker)delegate() { this.SafeProgressBarStyle = value; });
				}
				else
				{
					prgWait.Style = value;
					Application.DoEvents();
				}
			}
		}

		public int SafeProgressBarMaximum
		{
			get
			{
				int cVal = 0;

				if (this.InvokeRequired)
					this.Invoke((MethodInvoker)delegate() { cVal = prgWait.Maximum; });
				else
					cVal = prgWait.Maximum;

				return cVal;
			}
			set
			{
				if (this.InvokeRequired)
				{
					this.Invoke((MethodInvoker)delegate() { this.SafeProgressBarMaximum = value; });
				}
				else
				{
					prgWait.Maximum = value;
					Application.DoEvents();
				}
			}
		}

		public int SafeProgressBarValue
		{
			get
			{
				int cVal = 0;

				if (this.InvokeRequired)
					this.Invoke((MethodInvoker)delegate() { cVal = prgWait.Value; });
				else
					cVal = prgWait.Value;

				return cVal;
			}
			set
			{
				if (this.InvokeRequired)
				{
					this.Invoke((MethodInvoker)delegate() { this.SafeProgressBarValue = value; });
				}
				else
				{
					if (value > prgWait.Maximum)
						return;

					prgWait.Value = value;
					Application.DoEvents();
				}
			}

		}
	}
}
