﻿namespace PrEmpDoeEr
{
  partial class FrmMain
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmMain));
      this.gbMain = new System.Windows.Forms.GroupBox();
      this.txtSaveLoc = new System.Windows.Forms.TextBox();
      this.dtpTermDate = new System.Windows.Forms.DateTimePicker();
      this.tbExcludeTerm = new System.Windows.Forms.CheckBox();
      this.rbEvalNo = new System.Windows.Forms.RadioButton();
      this.rbEvalYes = new System.Windows.Forms.RadioButton();
      this.lblPrintMode = new System.Windows.Forms.Label();
      this.rbAll = new System.Windows.Forms.RadioButton();
      this.tbLoc = new System.Windows.Forms.CheckBox();
      this.lbSave = new System.Windows.Forms.Label();
      this.btBrowse = new System.Windows.Forms.Button();
      this.cbYear = new System.Windows.Forms.ComboBox();
      this.lbYear = new System.Windows.Forms.Label();
      this.cbEmpGrp = new System.Windows.Forms.ComboBox();
      this.lbGrp = new System.Windows.Forms.Label();
      this.cbLoc = new System.Windows.Forms.ComboBox();
      this.lbLoc = new System.Windows.Forms.Label();
      this.btCancel = new System.Windows.Forms.Button();
      this.btOk = new System.Windows.Forms.Button();
      this.bwMain = new System.ComponentModel.BackgroundWorker();
      this.cbPayGrp = new System.Windows.Forms.ComboBox();
      this.lbPayGrp = new System.Windows.Forms.Label();
      this.gbMain.SuspendLayout();
      this.SuspendLayout();
      // 
      // gbMain
      // 
      this.gbMain.Controls.Add(this.cbPayGrp);
      this.gbMain.Controls.Add(this.lbPayGrp);
      this.gbMain.Controls.Add(this.txtSaveLoc);
      this.gbMain.Controls.Add(this.dtpTermDate);
      this.gbMain.Controls.Add(this.tbExcludeTerm);
      this.gbMain.Controls.Add(this.rbEvalNo);
      this.gbMain.Controls.Add(this.rbEvalYes);
      this.gbMain.Controls.Add(this.lblPrintMode);
      this.gbMain.Controls.Add(this.rbAll);
      this.gbMain.Controls.Add(this.tbLoc);
      this.gbMain.Controls.Add(this.lbSave);
      this.gbMain.Controls.Add(this.btBrowse);
      this.gbMain.Controls.Add(this.cbYear);
      this.gbMain.Controls.Add(this.lbYear);
      this.gbMain.Controls.Add(this.cbEmpGrp);
      this.gbMain.Controls.Add(this.lbGrp);
      this.gbMain.Controls.Add(this.cbLoc);
      this.gbMain.Controls.Add(this.lbLoc);
      this.gbMain.Location = new System.Drawing.Point(6, -1);
      this.gbMain.Name = "gbMain";
      this.gbMain.Size = new System.Drawing.Size(330, 294);
      this.gbMain.TabIndex = 0;
      this.gbMain.TabStop = false;
      // 
      // txtSaveLoc
      // 
      this.txtSaveLoc.Location = new System.Drawing.Point(102, 261);
      this.txtSaveLoc.Name = "txtSaveLoc";
      this.txtSaveLoc.ReadOnly = true;
      this.txtSaveLoc.Size = new System.Drawing.Size(190, 20);
      this.txtSaveLoc.TabIndex = 15;
      // 
      // dtpTermDate
      // 
      this.dtpTermDate.CustomFormat = "MM/dd/yyyy";
      this.dtpTermDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
      this.dtpTermDate.Location = new System.Drawing.Point(120, 173);
      this.dtpTermDate.Name = "dtpTermDate";
      this.dtpTermDate.Size = new System.Drawing.Size(84, 20);
      this.dtpTermDate.TabIndex = 8;
      // 
      // tbExcludeTerm
      // 
      this.tbExcludeTerm.AutoSize = true;
      this.tbExcludeTerm.Location = new System.Drawing.Point(102, 150);
      this.tbExcludeTerm.Name = "tbExcludeTerm";
      this.tbExcludeTerm.Size = new System.Drawing.Size(204, 17);
      this.tbExcludeTerm.TabIndex = 7;
      this.tbExcludeTerm.Text = "Exclude employees terminated prior to";
      this.tbExcludeTerm.UseVisualStyleBackColor = true;
      this.tbExcludeTerm.CheckedChanged += new System.EventHandler(this.tbExclude_CheckedChanged);
      // 
      // rbEvalNo
      // 
      this.rbEvalNo.AutoSize = true;
      this.rbEvalNo.Location = new System.Drawing.Point(102, 238);
      this.rbEvalNo.Name = "rbEvalNo";
      this.rbEvalNo.Size = new System.Drawing.Size(165, 17);
      this.rbEvalNo.TabIndex = 12;
      this.rbEvalNo.TabStop = true;
      this.rbEvalNo.Text = "Employees without evaluation";
      this.rbEvalNo.UseVisualStyleBackColor = true;
      // 
      // rbEvalYes
      // 
      this.rbEvalYes.AutoSize = true;
      this.rbEvalYes.Location = new System.Drawing.Point(102, 219);
      this.rbEvalYes.Name = "rbEvalYes";
      this.rbEvalYes.Size = new System.Drawing.Size(150, 17);
      this.rbEvalYes.TabIndex = 11;
      this.rbEvalYes.TabStop = true;
      this.rbEvalYes.Text = "Employees with evaluation";
      this.rbEvalYes.UseVisualStyleBackColor = true;
      // 
      // lblPrintMode
      // 
      this.lblPrintMode.AutoSize = true;
      this.lblPrintMode.Location = new System.Drawing.Point(68, 202);
      this.lblPrintMode.Name = "lblPrintMode";
      this.lblPrintMode.Size = new System.Drawing.Size(28, 13);
      this.lblPrintMode.TabIndex = 9;
      this.lblPrintMode.Text = "Print";
      // 
      // rbAll
      // 
      this.rbAll.AutoSize = true;
      this.rbAll.Location = new System.Drawing.Point(102, 200);
      this.rbAll.Name = "rbAll";
      this.rbAll.Size = new System.Drawing.Size(36, 17);
      this.rbAll.TabIndex = 10;
      this.rbAll.TabStop = true;
      this.rbAll.Text = "All";
      this.rbAll.UseVisualStyleBackColor = true;
      // 
      // tbLoc
      // 
      this.tbLoc.AutoSize = true;
      this.tbLoc.Location = new System.Drawing.Point(102, 73);
      this.tbLoc.Name = "tbLoc";
      this.tbLoc.Size = new System.Drawing.Size(112, 17);
      this.tbLoc.TabIndex = 4;
      this.tbLoc.Text = "Break by Location";
      this.tbLoc.UseVisualStyleBackColor = true;
      // 
      // lbSave
      // 
      this.lbSave.AutoSize = true;
      this.lbSave.Location = new System.Drawing.Point(20, 264);
      this.lbSave.Name = "lbSave";
      this.lbSave.Size = new System.Drawing.Size(76, 13);
      this.lbSave.TabIndex = 13;
      this.lbSave.Text = "Save Location";
      // 
      // btBrowse
      // 
      this.btBrowse.Image = global::PrEmpDoeEr.Properties.Resources.BrowseBtn;
      this.btBrowse.Location = new System.Drawing.Point(294, 259);
      this.btBrowse.Name = "btBrowse";
      this.btBrowse.Size = new System.Drawing.Size(23, 23);
      this.btBrowse.TabIndex = 14;
      this.btBrowse.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
      this.btBrowse.Click += new System.EventHandler(this.btBrowse_Click);
      // 
      // cbYear
      // 
      this.cbYear.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
      this.cbYear.FormattingEnabled = true;
      this.cbYear.Location = new System.Drawing.Point(102, 19);
      this.cbYear.Name = "cbYear";
      this.cbYear.Size = new System.Drawing.Size(66, 21);
      this.cbYear.TabIndex = 1;
      // 
      // lbYear
      // 
      this.lbYear.AutoSize = true;
      this.lbYear.Location = new System.Drawing.Point(49, 22);
      this.lbYear.Name = "lbYear";
      this.lbYear.Size = new System.Drawing.Size(47, 13);
      this.lbYear.TabIndex = 0;
      this.lbYear.Text = "PR Year";
      // 
      // cbEmpGrp
      // 
      this.cbEmpGrp.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
      this.cbEmpGrp.FormattingEnabled = true;
      this.cbEmpGrp.Location = new System.Drawing.Point(102, 96);
      this.cbEmpGrp.Name = "cbEmpGrp";
      this.cbEmpGrp.Size = new System.Drawing.Size(215, 21);
      this.cbEmpGrp.TabIndex = 6;
      // 
      // lbGrp
      // 
      this.lbGrp.AutoSize = true;
      this.lbGrp.Location = new System.Drawing.Point(13, 99);
      this.lbGrp.Name = "lbGrp";
      this.lbGrp.Size = new System.Drawing.Size(85, 13);
      this.lbGrp.TabIndex = 5;
      this.lbGrp.Text = "Employee Group";
      // 
      // cbLoc
      // 
      this.cbLoc.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
      this.cbLoc.FormattingEnabled = true;
      this.cbLoc.Location = new System.Drawing.Point(102, 46);
      this.cbLoc.Name = "cbLoc";
      this.cbLoc.Size = new System.Drawing.Size(215, 21);
      this.cbLoc.TabIndex = 3;
      this.cbLoc.SelectedValueChanged += new System.EventHandler(this.cbLoc_SelectedValueChanged);
      // 
      // lbLoc
      // 
      this.lbLoc.AutoSize = true;
      this.lbLoc.Location = new System.Drawing.Point(48, 49);
      this.lbLoc.Name = "lbLoc";
      this.lbLoc.Size = new System.Drawing.Size(48, 13);
      this.lbLoc.TabIndex = 2;
      this.lbLoc.Text = "Location";
      // 
      // btCancel
      // 
      this.btCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
      this.btCancel.Image = global::PrEmpDoeEr.Properties.Resources.CancelBt_14;
      this.btCancel.Location = new System.Drawing.Point(171, 299);
      this.btCancel.Name = "btCancel";
      this.btCancel.Size = new System.Drawing.Size(75, 23);
      this.btCancel.TabIndex = 2;
      this.btCancel.Text = "&Cancel";
      this.btCancel.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
      this.btCancel.Click += new System.EventHandler(this.btCancel_Click);
      // 
      // btOk
      // 
      this.btOk.Image = global::PrEmpDoeEr.Properties.Resources.SaveBt_14;
      this.btOk.Location = new System.Drawing.Point(96, 299);
      this.btOk.Name = "btOk";
      this.btOk.Size = new System.Drawing.Size(75, 23);
      this.btOk.TabIndex = 1;
      this.btOk.Text = "&Ok";
      this.btOk.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
      this.btOk.Click += new System.EventHandler(this.btOk_Click);
      // 
      // bwMain
      // 
      this.bwMain.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bwMain_DoWork);
      this.bwMain.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.bwMain_RunWorkerCompleted);
      // 
      // cbPayGrp
      // 
      this.cbPayGrp.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
      this.cbPayGrp.FormattingEnabled = true;
      this.cbPayGrp.Location = new System.Drawing.Point(102, 123);
      this.cbPayGrp.Name = "cbPayGrp";
      this.cbPayGrp.Size = new System.Drawing.Size(215, 21);
      this.cbPayGrp.TabIndex = 17;
      // 
      // lbPayGrp
      // 
      this.lbPayGrp.AutoSize = true;
      this.lbPayGrp.Location = new System.Drawing.Point(39, 126);
      this.lbPayGrp.Name = "lbPayGrp";
      this.lbPayGrp.Size = new System.Drawing.Size(57, 13);
      this.lbPayGrp.TabIndex = 16;
      this.lbPayGrp.Text = "Pay Group";
      // 
      // FrmMain
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(343, 333);
      this.Controls.Add(this.btCancel);
      this.Controls.Add(this.btOk);
      this.Controls.Add(this.gbMain);
      this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
      this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
      this.MaximizeBox = false;
      this.Name = "FrmMain";
      this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
      this.Text = "Create DOE-ER File";
      this.Load += new System.EventHandler(this.FrmMain_Load);
      this.gbMain.ResumeLayout(false);
      this.gbMain.PerformLayout();
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.GroupBox gbMain;
    private System.Windows.Forms.ComboBox cbYear;
    private System.Windows.Forms.Label lbYear;
    private System.Windows.Forms.ComboBox cbEmpGrp;
    private System.Windows.Forms.Label lbGrp;
    private System.Windows.Forms.ComboBox cbLoc;
    private System.Windows.Forms.Label lbLoc;
    private System.Windows.Forms.Button btCancel;
    private System.Windows.Forms.Button btOk;
    private System.Windows.Forms.Button btBrowse;
    private System.Windows.Forms.Label lbSave;
    private System.ComponentModel.BackgroundWorker bwMain;
    private System.Windows.Forms.CheckBox tbLoc;
    private System.Windows.Forms.RadioButton rbEvalNo;
    private System.Windows.Forms.RadioButton rbEvalYes;
    private System.Windows.Forms.Label lblPrintMode;
    private System.Windows.Forms.RadioButton rbAll;
    private System.Windows.Forms.DateTimePicker dtpTermDate;
    private System.Windows.Forms.CheckBox tbExcludeTerm;
    private System.Windows.Forms.TextBox txtSaveLoc;
    private System.Windows.Forms.ComboBox cbPayGrp;
    private System.Windows.Forms.Label lbPayGrp;
  }
}

