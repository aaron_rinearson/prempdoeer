﻿using System;
using System.Windows.Forms;

namespace PrEmpDoeEr
{
	static class Program
	{
		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main(string[] args)
		{
		  if (System.Diagnostics.Debugger.IsAttached)
		    args = new[] { "FMS" };

			if (args.Length == 0)
			{
				MessageBox.Show("Incorrect number of arguments were passed. Please pass the database name as the only argument.");
				Environment.Exit(1);
			}

			Application.EnableVisualStyles();
			Application.SetCompatibleTextRenderingDefault(false);
			Application.Run(new FrmMain(args));
		}
	}
}
